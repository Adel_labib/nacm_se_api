﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NACM_SE_API.Models;


namespace NACM_SE_API.Controllers
{
    [ApiController]
    [Route("api/v1")]
    public class Caller: ControllerBase
    {
        [HttpPost("login")]
        public ActionResult<CmsValidation> Login([FromBody] CmsLogin data)
        {
            if (data == null) return BadRequest();

            string ret = Utils.Caller.sendRequestToNACM(new RequestLogin(data).XML);
            
            return Ok(new CmsValidation(ret));
        }

        [HttpPost("company-lookup")]
        public ActionResult<CmsCRCompanyList> CompanyLookup([FromBody] CmsCRCompanyLookup data)
        {
            if (data == null) return BadRequest();

            string ret = Utils.Caller.sendRequestToNACM(new RequestCompanyLookup(data).XML);

            return Ok(new CmsCRCompanyList(ret));
        }
        /*
        [HttpPost("report-detail")]
        public ActionResult<CmsCRCompanyList> ReportDetail([FromBody] CmsCRReportRequest data)
        {
            if (data == null) return BadRequest();

            string ret = Utils.Caller.sendRequestToNACM(new RequestReport(data).XML);

            return Ok(new CmsCRCompanyList(ret));
        }
        */
    }
}
