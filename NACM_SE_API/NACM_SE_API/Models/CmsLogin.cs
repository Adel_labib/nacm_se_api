﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NACM_SE_API.Models
{
    public class CmsLogin
    {
        public string AccessCode { get; set; } = "";
        public string UserRef { get; set; } = "";
    }
}
