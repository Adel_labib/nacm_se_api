﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NACM_SE_API.Models
{
    public class CmsCRScoreDetail
    {
        public string CompanyNACMID { get; set; } = "";
        public string Score { get; set; } = "";
        public string Class { get; set; } = "";
        public string ClassType { get; set; } = "";
        public string Message { get; set; } = "";
    }
}
