﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NACM_SE_API.Models
{
    public class CmsCRReportDetail
    {
        public string CompanyNACMID { get; set; } = "";
        public string CRSubject { get; set; } = "";
        public string Name { get; set; } = "";
        public string Addr1 { get; set; } = "";
        public string Addr2 { get; set; } = "";
        public string City { get; set; } = "";
        public string State { get; set; } = "";
        public string Zip { get; set; } = "";
        public string Phone { get; set; } = "";
        public string InFile { get; set; } = "";
        public CmsCRReportDetail_MiscComments MiscComments { get; set; }
        public CmsCRReportDetail_Corporate Corporate { get; set; }
        public CmsCRReportDetail_Officer Officer { get; set; }
        public CmsCRReportDetail_CRInqHist CRInqHist { get; set; }
        public CmsCRReportDetail_Tradeline Tradeline { get; set; }
        public CmsCRReportDetail_Trends Trends { get; set; }
        public CmsCRReportDetail_PublicRecords PublicRecords { get; set; }
        public CmsCRReportDetail_Bankruptcy Bankruptcy { get; set; }
        public CmsCRReportDetail_Alert Alert { get; set; }
        public CmsCRReportDetail_UCCFilings UCCFilings { get; set; }
        public CmsCRReportDetail_FinancialInstitution FinancialInstitution { get; set; }
        public CmsCRReportDetail_Claims Claims { get; set; }
        public CmsCRReportDetail_RequestingMember RequestingMember { get; set; }
        public string Message { get; set; } = "";
    }

    public class CmsCRReportDetail_MiscComments
    {
        public int SeqNo { get; set; }
        public string Text { get; set; } = "";
    }
    public class CmsCRReportDetail_Corporate
    {
        public string CorporateName { get; set; } = "";
        public string CorporateAddress { get; set; } = "";
        public DateTime IncorporatedDate { get; set; }
        public string CorporateStatus { get; set; } = "";
        public DateTime FileDate { get; set; }
        public string DocumentNo { get; set; } = "";
        public DateTime Updated { get; set; }
        public string Agent { get; set; } = "";
        public string Comment { get; set; } = "";
    }
    public class CmsCRReportDetail_Officer
    {
        public string Title { get; set; } = "";
        public string Name { get; set; } = "";
        public string Address{ get; set; } = "";
    }
    public class CmsCRReportDetail_CRInqHist
    {
        public string Industry { get; set; } = "";
        public DateTime Date{ get; set; }
    }
    public class CmsCRReportDetail_Tradeline
    {
        public string SourceID { get; set; }
        public string IndType { get; set; }
        public string OpenDate { get; set; }
        public string LastSale { get; set; }
        public DateTime ReportDate { get; set; }
        public long TotalDue { get; set; }
        public long HighCredit { get; set; }
        public long Current { get; set; }
        public long PD30Day { get; set; }
        public long PD60Day { get; set; }
        public long PD90Day { get; set; }
        public long PD120Day { get; set; }
        public string Comment{ get; set; }
        public string AvgDays { get; set; }
        public string Region { get; set; }
    }
    public class CmsCRReportDetail_Trends
    {
        public string Period { get; set; }
        public string LineCnt { get; set; }
        public long TotalDue { get; set; }
        public string CurrentPct { get; set; }
        public string PD30DayPct { get; set; }
        public string PD60DayPct { get; set; }
        public string PD90DayPct { get; set; }
        public string PD120DayPct { get; set; }        
    }
    public class CmsCRReportDetail_PublicRecords
    {
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string County { get; set; }
        public string Book { get; set; }
        public string Page { get; set; }
        public string Lienor { get; set; }
        public long Amount{ get; set; }
        public string Property { get; set; }
        public DateTime SatisDate { get; set; }
        public string SatisBook { get; set; }
        public string SatisPage { get; set; }
    }
    public class CmsCRReportDetail_Bankruptcy
    {
        public DateTime Date { get; set; }
        public string Case { get; set; }
        public string Chapter { get; set; }
        public string Attorney { get; set; }
        public string Comment { get; set; }
    }
    public class CmsCRReportDetail_Alert
    {
        public string SourceID{ get; set; }
        public string IndType { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public float Amount { get; set; }
        public string Comment { get; set; }
        public string Region { get; set; }
    }
    public class CmsCRReportDetail_UCCFilings
    {
        public string Account { get; set; }
        public DateTime Date { get; set; }
        public DateTime Expires { get; set; }
        public string Security { get; set; }
        public string SecurityAddress { get; set; }
    }
    public class CmsCRReportDetail_FinancialInstitution
    {
        public string Source { get; set; }
        public string OpenSince { get; set; }
        public string AcctType { get; set; }
        public DateTime ReportDate { get; set; }
        public string OpenFigures { get; set; }
        public string CurrFigures { get; set; }
        public string Comment { get; set; }
    }
    public class CmsCRReportDetail_Claims
    {
        public string MemberNo { get; set; }
        public string IndType { get; set; }
        public DateTime EntryDate { get; set; }
        public string Status { get; set; }
        public string ClaimAmt { get; set; }
        public string ClaimBalance { get; set; }
        public string Region { get; set; }
    }
    public class CmsCRReportDetail_RequestingMember
    {
        public string MemberNo { get; set; }
        public string Name { get; set; }
        public string Addr1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
